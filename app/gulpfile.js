var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

gulp.task('styles', function() {
    gulp.src('assets/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/'))
        .pipe(browserSync.stream());
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch("assets/*.scss", ['styles']);
    gulp.watch("./*.html").on('change', browserSync.reload);
});